package tools

import (
	"fmt"
	"google.golang.org/grpc"
	"log"
)

func CreateConnection(address string, desc *grpc.ServiceDesc) *grpc.ClientConn {
	//altsTC := alts.NewClientCreds(alts.DefaultClientOptions())
	// , grpc.WithTransportCredentials(altsTC)
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatal(fmt.Sprintf("Can't connect to %s microservice. Error: %s", desc.ServiceName, err))
	}
	//defer conn.Close()

	return conn
}
