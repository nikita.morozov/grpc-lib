package tools

import (
	"google.golang.org/grpc/metadata"
	"regexp"
)

func GetDataFromContext(md metadata.MD, key string) *string {
	r := regexp.MustCompile("[^\\s]+")

	if len(md[key]) == 0 {
		return nil
	}

	res := r.FindAllString(md[key][0], -1)
	if len(res[0]) == 0 {
		return nil
	}

	return &res[0]
}
